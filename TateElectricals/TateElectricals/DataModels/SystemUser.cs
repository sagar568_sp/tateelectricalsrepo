﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using TateElectricals.PasswordHelper;

namespace TateElectricals.DataModels
{
    public enum UserType { MasterAdmin,Admin,TerminalUser,ServiceUser}
    
    public class SystemUser : BaseEntity
    {
        public bool SetPassword(string password)
        {
            if (password != null)
            {
                this.PasswordHash = PasswordHelper.PasswordHelper.HashPassword(password);
                return true;
            }
            else return false;
        }

        public bool ValidatePassword(string hashedPassword, string providedPassword)
        {
            //var temp = PasswordHelper.PasswordHelper.HashPassword(providedPassword);
            return PasswordHelper.PasswordHelper.VerifyHashedPassword( hashedPassword, providedPassword);
        }
      

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int SystemUserId { set; get; }

        [Required,MaxLength ( 30)]
        public string UserName { get; set; }

        [Required, MaxLength(200)]
        public string PasswordHash { get; private set; }

        [NotMapped, MaxLength(50)]
        public string Password { get; set; }

        [ForeignKey("DepartmentId")]
        public int? DepartmentId { set; get; }
        public Department department { set; get; }

        [ForeignKey("ClientInfoId")]
        public int ClientInfoId { set; get; }
        public ClientInfo clientInfo { set; get; }
    }
}
