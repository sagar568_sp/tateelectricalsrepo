﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace TateElectricals.DataModels
{
    public enum TranfomserStatus { ForService, UnderService, ServiceDone }
    public class GatePassTranfomerConfiguration : BaseEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int GatePassTranfomerConfigurationId { set; get; }

        [ForeignKey("GatePassDetailId")]
        public int GatePassDetailId { set; get; }
        public GatePassDetail gatePassDetail { set; get; }

        [ForeignKey("TranformerDetailId")]
        public int TranformerDetailId { set; get; }
        public TranformerDetail tranformerDetail { set; get; }

        [ForeignKey("LastDepartmentId")]
        public int LastDepartmentId { set; get; }
        public Department lastDepartment { set; get; }

        [ForeignKey("CurrentDepartmentId")]
        public int? CurrentDepartmentId { set; get; }
        public Department CurrentDepartment { set; get; }

        public TranfomserStatus TranfomserStatus { get; set; }

        public bool IsTrasfomerWithoutOil {get;set;}

        public int OilSupplied { set; get; }

       
        public int TrasfomerJob_no { get; set; }
    }
}
