﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace TateElectricals.DataModels
{
    public class ClientInfo : BaseEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ClientInfoId { set; get; }

        [Required, MaxLength(50)]
        public string ClientName { get; set; }

        [Required, MaxLength(100)]
        public string ClientAddress { get; set; }

        [ MaxLength(25)]
        public string ClientCity { get; set; }

        [MaxLength(100)]
        public string ClientEmail { get; set; }

        [MaxLength(15)]
        public int ClientContact { get; set; }

        [Required, MaxLength(3)]
        public int ClientLicense { get; set; }
        public byte[] ClientLogo { get; set; }
    }
}
