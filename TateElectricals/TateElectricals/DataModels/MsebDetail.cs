﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace TateElectricals.DataModels
{
    public class MsebDetail : BaseEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int MsebDetailID { set; get; }

        [Required, MaxLength(15)]
        public string MsebCode { get; set; }

        [Required, MaxLength(100)]
        public string MsebName { get; set; }

        [MaxLength(100)]
        public string MsebAddress { get; set; }

        [MaxLength(25)]
        public string MsebCity { get; set; }

        [MaxLength(50)]
        public string MsebEmail { get; set; }

        [MaxLength(15)]
        public string MsebContact { get; set; }
        [MaxLength(200)]
        public string MsebLocation { get; set; }

    }
}
