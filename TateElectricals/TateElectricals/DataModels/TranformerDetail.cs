﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace TateElectricals.DataModels
{

    public class TranformerDetail : BaseEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int TranformerDetailsId { set; get; }

        [Required, MaxLength(25)]
        public string TransformerType { get; set; }

        public int TransformerOilCapacity { get; set; }

        [Required, MaxLength(50)]
        public string TranfomerMake { get; set; }

        public string TransfomerSr_no { get; set; }

        public int TransfomerService_no { get; set; }

        [ForeignKey("TranformerWarratyDetailId")]
        public int? TranformerWarratyDetailId { set; get; }
        public TranformerWarratyDetail tranformerWarratyDetail { set; get; }

      //Values for Transformer Configuration
      [NotMapped]
        public int? LastDepartmentId { set; get; }
        [NotMapped]
        public Department lastDepartment { set; get; }
        [NotMapped]

        public int? CurrentDepartmentId { set; get; }
        [NotMapped]
        public Department CurrentDepartment { set; get; }
        [NotMapped]
        public TranfomserStatus TranfomserStatus { get; set; }
        [NotMapped]
        public bool IsTrasfomerWithoutOil { get; set; }
        [NotMapped]
        public int OilSupplied { set; get; }       
        
        [NotMapped]
        public string TrasfomerJob_no { get; set; }


    }
}
