﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace TateElectricals.DataModels
{
    public class OilDetail : BaseEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int? OilDetailsId { set; get; }

        [ForeignKey("GatePassDetailId")]
        public int? GatePassDetailId { set; get; }
        public GatePassDetail gatePassDetail { set; get; }

        public int TotalOilRecieved { set; get; }

        public int TotalOil { get; set; }
        public int TotalOilRequird { set; get; }


        [ForeignKey("OilPassDetailId")]
        public int? OilPassDetailId { set; get; }
        public OilPassDetail oilPassDetail { set; get; }

    }
}
