﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace TateElectricals.DataModels
{
   public enum BaseState { UnChanged,Added,Modified,Deleted}
    public class BaseEntity
    {
        [NotMapped]
        public BaseState EntityState { set; get; }
    }
}
