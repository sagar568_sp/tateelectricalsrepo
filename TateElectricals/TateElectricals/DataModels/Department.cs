﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace TateElectricals.DataModels
{
    public enum DepartmentType { Admin,Agency,Service}
    public class Department : BaseEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int DepartmentId { set; get; }

        [Required, MaxLength(15)]
        public string DeptCode { get; set; }

        [Required, MaxLength(50)]
        public string DeptName { get; set; }
       // public byte[] DeptLogo { get; set; }

        [ MaxLength(100)]
        public string DeptAddress { get; set; }

        [ MaxLength(25)]
        public string DeptCity { get; set; }

        [MaxLength(50)]
        public string DeptEmail { get; set; }

        [MaxLength(15)]
        public string DeptContact { get; set; }
        public DepartmentType DepartmentType { get; set; }

        //[MaxLength(15)]
        //public int SystemUserID { get; set; }



    }
}
