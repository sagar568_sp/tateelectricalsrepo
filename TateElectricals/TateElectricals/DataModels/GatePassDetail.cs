﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace TateElectricals.DataModels
{
    public class GatePassDetail : BaseEntity
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int GatePassDetailId { set; get; }

        public int GatePassSr_no { get; set; }

        [Required]
        public DateTime GatePassDate { get; set; }

        [StringLength(100)]
        public string GatePassRemark { get; set; }

        [Required, StringLength(100)]
        public string GatePassReciversName { get; set; }

        [StringLength(100)]
        public string GatePassSendersName { get; set; }

        [StringLength(50)]
        public string GatePassVehicleNo { get; set; }

        [ForeignKey("DepartmentId")]
        public int DepartmentId { set; get; }
        public Department department { set; get; }

        [ForeignKey("MsebDetailId")]
        public int MsebDetailId { set; get; }
        public MsebDetail msebDetail { set; get; }

        public bool IsGatePassWithoutOil { set; get; }

    }
}
