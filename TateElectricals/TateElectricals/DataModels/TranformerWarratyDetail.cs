﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace TateElectricals.DataModels
{
    public enum WarrantyStatus { UnderWarraty, Expired, NotApplicable }
    public class TranformerWarratyDetail : BaseEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int TranformerWarratyDetailId { set; get; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public WarrantyStatus GetWarrantyStatus()
        {
            if (this.StartDate == null && this.EndDate == null)
            {
                return WarrantyStatus.NotApplicable;
            }
            else if (this.StartDate < DateTime.Now && DateTime.Now < this.EndDate)
            {
                return WarrantyStatus.UnderWarraty;
            }
            else if (this.StartDate < DateTime.Now && DateTime.Now > this.EndDate)
            {
                return WarrantyStatus.Expired;
            }
            else
                return WarrantyStatus.NotApplicable;
        }

    }
}
