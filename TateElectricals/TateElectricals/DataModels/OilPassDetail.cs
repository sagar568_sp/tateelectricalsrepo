﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace TateElectricals.DataModels
{
    public class OilPassDetail : BaseEntity

    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int OilPassDetailId { set; get; }

        public int OilPassSr_no { get; set; }

        [Required]
        public DateTime OilPassDate { get; set; }

        [StringLength(100)]
        public string OilPassRemark { get; set; }

        [Required, StringLength(100)]
        public string OilPassReciversName { get; set; }

        [StringLength(100)]
        public string OilPassSendersName { get; set; }

        [StringLength(50)]
        public string OilPassVehicleNo { get; set; }

        [ForeignKey("DepartmentId")]
        public int DepartmentId { set; get; }
        public Department department { set; get; }

        [ForeignKey("MsebDetailId")]
        public int MsebDetailId { set; get; }
        public MsebDetail msebDetail { set; get; }

    }
}
