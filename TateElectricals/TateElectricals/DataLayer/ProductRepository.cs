﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TateElectricals.DataModels;

namespace TateElectricals.DataLayer
{
    public class ProductRepository : IProductRepository
    {
        private readonly ClientContext clientContext;
        public ProductRepository(ClientContext context)
        {
            this.clientContext = context;
        }

        public ClientInfo GetClientInfo(int ClientInfoId)
        {

            throw new NotImplementedException();
        }

        public object GetSystemUser(int id)
        {
            try
            {
                var ss = from dp in id == 0 ? clientContext.SystemUsers.Include("department").Where(s => s.SystemUserId != 1).ToList() : clientContext.SystemUsers.Include("department").Where(u => u.SystemUserId == id).ToList()
                         select new
                         {
                             DepartmentId = dp.department.DepartmentId,
                             DeptCode = dp.department.DeptCode,
                             DeptName = dp.department.DeptName,
                             DeptAddress = dp.department.DeptAddress,
                             DeptCity = dp.department.DeptCity,
                             DeptEmail = dp.department.DeptEmail,
                             DeptContact = dp.department.DeptContact,
                             SystemUserID = dp.SystemUserId,
                             departmentType = dp.department.DepartmentType
                         };

                return ss.ToList();
            }
            catch (Exception ex)
            {
                LogHelper.LogHelper.ExceptionError(ex);
                return null;
            }
        }

        public List<MsebDetail> GetMSED()
        {
            try
            {
                return clientContext.MsebDetails.ToList();
            }
            catch (Exception ex)
            {
                LogHelper.LogHelper.ExceptionError(ex);
                return null;
            }
        }

        public bool SaveSystemUser(SystemUser systemUser)
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(systemUser.UserName) && !string.IsNullOrWhiteSpace(systemUser.Password))
                {
                    switch (systemUser.EntityState)
                    {
                        case BaseState.Deleted:
                            var objSytemUser = clientContext.SystemUsers.Where(s => s.SystemUserId == systemUser.SystemUserId).FirstOrDefault();
                            clientContext.SystemUsers.Remove(objSytemUser);
                            break;
                        case BaseState.Modified:
                            objSytemUser = clientContext.SystemUsers.Include("department").Where(s => s.SystemUserId == systemUser.SystemUserId).FirstOrDefault();
                            objSytemUser.UserName = systemUser.UserName;
                            if (!string.IsNullOrWhiteSpace(systemUser.Password))
                                objSytemUser.SetPassword(systemUser.Password);
                            objSytemUser.ClientInfoId = 12;
                            objSytemUser.DepartmentId = systemUser.DepartmentId;
                            switch (systemUser.department.EntityState)
                            {
                                case BaseState.Modified:
                                    objSytemUser.department.DeptCode = systemUser.department.DeptCode;
                                    objSytemUser.department.DepartmentType = systemUser.department.DepartmentType;
                                    objSytemUser.department.DeptAddress = systemUser.department.DeptAddress;
                                    objSytemUser.department.DeptCity = systemUser.department.DeptCity;
                                    objSytemUser.department.DeptContact = systemUser.department.DeptContact;
                                    objSytemUser.department.DeptEmail = systemUser.department.DeptEmail;
                                    objSytemUser.department.DeptName = systemUser.department.DeptName;
                                    break;
                            }
                            break;
                        case BaseState.Added:
                            systemUser.SystemUserId = GenrateID();
                            systemUser.department.DepartmentId = GenrateID();
                            systemUser.SetPassword(systemUser.Password);
                            clientContext.SystemUsers.Add(systemUser);
                            break;
                    }
                    clientContext.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                LogHelper.LogHelper.ExceptionError(ex);
            }
            return false;
        }

        public SystemUser ValidateSystemUser(string UserName, string password)
        {
            var userobj = clientContext.SystemUsers.Include("department").Where(s => s.UserName == UserName).ToList().FirstOrDefault();
            if (userobj != null)
            {
                if (userobj.ValidatePassword(userobj.PasswordHash, password))
                    return userobj;
                else
                    return null;
            }
            else return null;


        }
        public int GenrateID()
        {
            Random rnd = new Random();
            return rnd.Next(1000000, 9999999);
        }


        #region MSED
        public bool SaveMsebDetail(MsebDetail MsebDetail)
        {
            try
            {
                //vijay delete
                if (MsebDetail.EntityState == BaseState.Deleted)
                {
                    var objMsebDetail = clientContext.MsebDetails.Where(s => s.MsebDetailID == MsebDetail.MsebDetailID).FirstOrDefault();
                    clientContext.MsebDetails.Remove(objMsebDetail);
                    clientContext.SaveChanges();
                    return true;
                }
                if (!string.IsNullOrWhiteSpace(MsebDetail.MsebName) && !string.IsNullOrWhiteSpace(MsebDetail.MsebCode))
                {
                    switch (MsebDetail.EntityState)
                    {
                        case BaseState.Deleted:
                            var objMsebDetail = clientContext.MsebDetails.Where(s => s.MsebDetailID == MsebDetail.MsebDetailID).FirstOrDefault();
                            clientContext.MsebDetails.Remove(objMsebDetail);
                            //clientContext.SaveChanges();
                            //return true;
                            break;
                        case BaseState.Modified:
                            objMsebDetail = clientContext.MsebDetails.Where(s => s.MsebDetailID == MsebDetail.MsebDetailID).FirstOrDefault();
                            objMsebDetail.MsebName = MsebDetail.MsebName;
                            objMsebDetail.MsebCode = MsebDetail.MsebCode;
                            objMsebDetail.MsebAddress = MsebDetail.MsebAddress;
                            objMsebDetail.MsebCity = MsebDetail.MsebCity;
                            objMsebDetail.MsebEmail = MsebDetail.MsebEmail;
                            objMsebDetail.MsebContact = MsebDetail.MsebContact;

                            //objMsebDetail.DeptLogo = MsebDetail.DeptLogo;
                            //objSytemUser.
                            break;
                        case BaseState.Added:
                            // MsebDetail.MsebDetailId = 1245;
                            MsebDetail.MsebDetailID = GenrateID();
                            clientContext.MsebDetails.Add(MsebDetail);
                            break;
                    }

                    clientContext.SaveChanges();
                    return true;

                }
            }
            catch (Exception ex)
            {
                LogHelper.LogHelper.ExceptionError(ex);

            }
            return false;
        }

        #endregion


        #region GetPass

        public object GetTransformr(string Date = "", int JobNo = 0, int TokenNo = 0, int currentPage = 1, int PageSize = 5)
        {


            DateTime dt = Convert.ToDateTime(Date);
            List<GatePassTranfomerConfiguration> li = new List<GatePassTranfomerConfiguration>();

            int skipP = currentPage == 1 ? 0 : ((currentPage - 1) * (PageSize));
            int TakeP = (PageSize);

            int TotalCount = 0;
            var getli = from gtc in clientContext.GatePassTranfomerConfigurations.OrderByDescending(x => x.TrasfomerJob_no).Skip(skipP).Take(TakeP)
                        select new
                        {
                            GatePassTranfomerConfigurationId = gtc.GatePassTranfomerConfigurationId,
                            OilSupplied = gtc.OilSupplied,
                            IsTrasfomerWithoutOil = gtc.IsTrasfomerWithoutOil,
                            TranfomserStatus = gtc.TranfomserStatus,
                            TrasfomerJob_no = gtc.TrasfomerJob_no,
                            TranformerDetail = clientContext.TranformerDetails.FirstOrDefault(x => x.TranformerDetailsId == gtc.TranformerDetailId),
                            GatePassDetail = clientContext.GatePassDetails.FirstOrDefault(x => x.GatePassDetailId == gtc.GatePassDetailId),
                            Department = clientContext.Departments.FirstOrDefault(x => x.DepartmentId == gtc.LastDepartmentId)
                        };

            if ((Date != "0" && Date != "" && Date != null) || (JobNo != 0 && JobNo != null) || (TokenNo != 0 && TokenNo != null))
            {

                getli = from gtc in clientContext.GatePassTranfomerConfigurations.Where(s => s.gatePassDetail.GatePassDate == dt || s.TrasfomerJob_no == JobNo || s.gatePassDetail.GatePassSr_no == TokenNo).OrderByDescending(x => x.TrasfomerJob_no).Skip(skipP).Take(TakeP)
                        select new
                        {
                            GatePassTranfomerConfigurationId = gtc.GatePassTranfomerConfigurationId,
                            OilSupplied = gtc.OilSupplied,
                            IsTrasfomerWithoutOil = gtc.IsTrasfomerWithoutOil,
                            TranfomserStatus = gtc.TranfomserStatus,
                            TrasfomerJob_no = gtc.TrasfomerJob_no,
                            TranformerDetail = clientContext.TranformerDetails.FirstOrDefault(x => x.TranformerDetailsId == gtc.TranformerDetailId),
                            GatePassDetail = clientContext.GatePassDetails.FirstOrDefault(x => x.GatePassDetailId == gtc.GatePassDetailId),
                            Department = clientContext.Departments.FirstOrDefault(x => x.DepartmentId == gtc.LastDepartmentId)
                        };


            }
            TotalCount = clientContext.GatePassTranfomerConfigurations.Count();

            int v = TotalCount % PageSize;
            int TotalPages = TotalCount / PageSize;
            TotalPages = v == 0 ? TotalPages : (TotalPages+1);

            return (getli, TotalCount, TotalPages, PageSize);
        }
        public GatePassTranfomerConfiguration SaveGatePass(GatePassDetail GPDetail, TranformerDetail TranDetail, ref GatePassTranfomerConfiguration gtc)
        {
            int GatePassDetID = 0;
            try
            {
                if (!string.IsNullOrWhiteSpace(GPDetail.GatePassVehicleNo) && !string.IsNullOrWhiteSpace(GPDetail.GatePassReciversName))
                {
                    switch (GPDetail.EntityState)
                    {
                        case BaseState.Deleted:
                            var objGPDetail = clientContext.GatePassDetails.Where(s => s.GatePassDetailId == GPDetail.GatePassDetailId).FirstOrDefault();
                            clientContext.GatePassDetails.Remove(objGPDetail);
                            //clientContext.SaveChanges();
                            //return true;
                            break;
                        case BaseState.Modified:

                            objGPDetail = clientContext.GatePassDetails.Where(s => s.GatePassDetailId == GPDetail.GatePassDetailId).FirstOrDefault();
                            objGPDetail.GatePassSr_no = GPDetail.GatePassSr_no;
                            objGPDetail.GatePassDate = GPDetail.GatePassDate;
                            objGPDetail.GatePassRemark = GPDetail.GatePassRemark;
                            objGPDetail.GatePassReciversName = GPDetail.GatePassReciversName;
                            objGPDetail.GatePassSendersName = GPDetail.GatePassSendersName;
                            objGPDetail.GatePassVehicleNo = GPDetail.GatePassVehicleNo;
                            objGPDetail.DepartmentId = GPDetail.DepartmentId;
                            objGPDetail.MsebDetailId = GPDetail.MsebDetailId;
                            objGPDetail.IsGatePassWithoutOil = GPDetail.IsGatePassWithoutOil;

                            //objMsebDetail.DeptLogo = MsebDetail.DeptLogo;
                            //objSytemUser.
                            break;
                        case BaseState.Added:
                            // MsebDetail.MsebDetailId = 1245;
                            GPDetail.GatePassDetailId = GenrateID();
                            clientContext.GatePassDetails.Add(GPDetail);
                            clientContext.SaveChanges();
                            break;
                    }
                    GatePassDetID = GPDetail.GatePassDetailId;


                    int trdID = SaveTransDetail(TranDetail, GatePassDetID);
                    TranDetail.TranformerDetailsId = trdID;
                    SaveGetPassTransConfiguration(TranDetail, ref gtc, GatePassDetID);

                    gtc.tranformerDetail = TranDetail;
                    gtc.gatePassDetail = GPDetail;

                    return gtc;

                }



                return gtc;
            }
            catch (Exception ex)
            {

                LogHelper.LogHelper.ExceptionError(ex);

            }
            return gtc;
        }
        private int SaveTransDetail(TranformerDetail TranDetail, int GatePassdetailID)
        {
            int TransDetID = 0;
            try
            {
                if (!string.IsNullOrWhiteSpace(TranDetail.TranfomerMake))
                {
                    switch (TranDetail.EntityState)
                    {
                        case BaseState.Deleted:
                            var objTranDetail = clientContext.TranformerDetails.Where(s => s.TranformerDetailsId == TranDetail.TranformerDetailsId).FirstOrDefault();
                            clientContext.TranformerDetails.Remove(objTranDetail);
                            //clientContext.SaveChanges();
                            //return true;
                            break;
                        case BaseState.Modified:
                            objTranDetail = clientContext.TranformerDetails.Where(s => s.TranformerDetailsId == TranDetail.TranformerDetailsId).FirstOrDefault();

                            objTranDetail.TransformerType = TranDetail.TransformerType;
                            objTranDetail.TransformerOilCapacity = TranDetail.TransformerOilCapacity;
                            objTranDetail.TranfomerMake = TranDetail.TranfomerMake;
                            objTranDetail.TransfomerSr_no = TranDetail.TransfomerSr_no;
                            objTranDetail.TransfomerService_no = TranDetail.TransfomerService_no;
                            objTranDetail.TranformerWarratyDetailId = TranDetail.TranformerWarratyDetailId;

                            TransDetID = TranDetail.TranformerDetailsId;
                            //objMsebDetail.DeptLogo = MsebDetail.DeptLogo;
                            //objSytemUser.
                            break;
                        case BaseState.Added:
                            // MsebDetail.MsebDetailId = 1245;
                            TranDetail.TranformerDetailsId = GenrateID();
                            clientContext.TranformerDetails.Add(TranDetail);
                            clientContext.SaveChanges();
                            TransDetID = TranDetail.TranformerDetailsId;
                            break;
                    }

                    return TransDetID;

                }



                return TransDetID;
            }
            catch (Exception ex)
            {

                LogHelper.LogHelper.ExceptionError(ex);

            }
            return TransDetID;



        }
        bool SaveGetPassTransConfiguration(TranformerDetail TranDetail, ref GatePassTranfomerConfiguration gtc, int GatePassdetailID)
        {

            try
            {
                int did = gtc.LastDepartmentId;
                var ss = clientContext.GatePassTranfomerConfigurations.Where(x => x.LastDepartmentId == did).OrderByDescending(x => x.TrasfomerJob_no);

                int jobid = ss.Count() > 0 ? Convert.ToInt32(ss.FirstOrDefault().TrasfomerJob_no) : 0;
                GatePassTranfomerConfiguration obj = new GatePassTranfomerConfiguration();
                obj.IsTrasfomerWithoutOil = gtc.IsTrasfomerWithoutOil;
                obj.lastDepartment = gtc.lastDepartment;
                obj.TranformerDetailId = TranDetail.TranformerDetailsId;
                obj.GatePassDetailId = GatePassdetailID;
                obj.LastDepartmentId = gtc.LastDepartmentId;
                obj.OilSupplied = gtc.OilSupplied;
                obj.TranfomserStatus = gtc.TranfomserStatus;
                obj.TrasfomerJob_no = (jobid + 1);

                switch (gtc.EntityState)
                {
                    case BaseState.Deleted:
                        var objConTranDetail = clientContext.GatePassTranfomerConfigurations.Where(s => s.TranformerDetailId == TranDetail.TranformerDetailsId && s.GatePassDetailId == GatePassdetailID).FirstOrDefault();
                        clientContext.GatePassTranfomerConfigurations.Remove(objConTranDetail);
                        //clientContext.SaveChanges();
                        //return true;
                        break;
                    case BaseState.Modified:
                        clientContext.SaveChanges();
                        //objMsebDetail.DeptLogo = MsebDetail.DeptLogo;
                        //objSytemUser.
                        break;
                    case BaseState.Added:
                        // MsebDetail.MsebDetailId = 1245;
                        obj.GatePassTranfomerConfigurationId = GenrateID();

                        clientContext.GatePassTranfomerConfigurations.Add(obj);
                        clientContext.SaveChanges();

                        break;


                }
                gtc = obj;
                OilDetail od = new OilDetail();
                od.EntityState = BaseState.Added;
                od.GatePassDetailId = GatePassdetailID;
                od.TotalOilRecieved = gtc.OilSupplied;
                od.TotalOilRequird = TranDetail.TransformerOilCapacity;
                od.TotalOil = gtc.OilSupplied; ;

                SaveOilDetails(od);

                return true;

            }
            catch (Exception ex)
            {

                LogHelper.LogHelper.ExceptionError(ex);

            }
            return false;
        }
        bool SaveOilDetails(OilDetail od)
        {

            try
            {

                switch (od.EntityState)
                {
                    case BaseState.Deleted:
                        var objConTranDetail = clientContext.OilDetails.Where(s => s.OilDetailsId == od.OilDetailsId && s.GatePassDetailId == od.GatePassDetailId).FirstOrDefault();
                        clientContext.OilDetails.Remove(od);

                        break;
                    case BaseState.Modified:
                        clientContext.SaveChanges();

                        break;
                    case BaseState.Added:

                       od.OilDetailsId = GenrateID();

                        clientContext.OilDetails.Add(od);
                        clientContext.SaveChanges();

                        break;
                }
                return true;

            }
            catch (Exception ex)
            {

                LogHelper.LogHelper.ExceptionError(ex);

            }
            return false;
        }

        #endregion

        #region OilPass

        public object GetOilDetails(int currentPage = 1, int PageSize = 0)
        {

            try
            {
                int skipP = currentPage == 1 ? 0 : ((currentPage - 1) * (PageSize));
                int TakeP = (PageSize);

                int TotalCount = 0;
                var od = from odl in clientContext.OilDetails.OrderBy(x=>x.oilPassDetail).GroupBy(x=>new { x.GatePassDetailId,x.OilPassDetailId }).Skip(skipP).Take(TakeP)

                         select new 
                         {
                             odl.Key.GatePassDetailId,
                             odl.Key.OilPassDetailId,
                             TotalOilRequird =odl.Sum(x=>x.TotalOilRequird) ,
                             TotalOilRecieved = odl.Sum(x => x.TotalOilRecieved)
                         };

                var gt = from d in od
                         select new
                         {
                             d.GatePassDetailId,
                             d.OilPassDetailId,
                             d.TotalOilRequird,
                             d.TotalOilRecieved,
                             GetPassDet=clientContext.GatePassDetails.FirstOrDefault(x=>x.GatePassDetailId== d.GatePassDetailId),
                             GatePassMSED= clientContext.GatePassDetails.FirstOrDefault(x => x.GatePassDetailId == d.GatePassDetailId).msebDetail,
                             OilPassMSED = clientContext.OilPasss.FirstOrDefault(x => x.OilPassDetailId == d.OilPassDetailId).msebDetail,
                             OilPassDet = clientContext.OilPasss.FirstOrDefault(x => x.OilPassDetailId == d.OilPassDetailId),
                         };
                TotalCount = clientContext.OilDetails.OrderBy(x => x.oilPassDetail).GroupBy(x => new { x.GatePassDetailId, x.OilPassDetailId }).Count();

                int v = TotalCount % PageSize;
                int TotalPages = TotalCount / PageSize;
                TotalPages = v == 0 ? TotalPages : (TotalPages + 1);

                return (gt, TotalCount, TotalPages, PageSize);
                
            }
            catch (Exception ex)
            {

                LogHelper.LogHelper.ExceptionError(ex);

            }
            return false;
        }
        public bool SaveOilPass(OilPassDetail OilPasss, int TotalOilRecieved)
        {
            int OilPassDetID = 0;
            try
            {
                if (!string.IsNullOrWhiteSpace(OilPasss.OilPassVehicleNo) && !string.IsNullOrWhiteSpace(OilPasss.OilPassReciversName))
                {
                    switch (OilPasss.EntityState)
                    {
                        case BaseState.Deleted:
                            var objOilPass = clientContext.OilPasss.Where(s => s.OilPassDetailId == OilPasss.OilPassDetailId).FirstOrDefault();
                            clientContext.OilPasss.Remove(objOilPass);
                            //clientContext.SaveChanges();
                            //return true;
                            break;
                        case BaseState.Modified:

                            objOilPass = clientContext.OilPasss.Where(s => s.OilPassDetailId == OilPasss.OilPassDetailId).FirstOrDefault();
                            objOilPass.OilPassSr_no = OilPasss.OilPassSr_no;
                            objOilPass.OilPassDate = OilPasss.OilPassDate;
                            objOilPass.OilPassRemark = OilPasss.OilPassRemark;
                            objOilPass.OilPassReciversName = OilPasss.OilPassReciversName;
                            objOilPass.OilPassSendersName = OilPasss.OilPassSendersName;
                            objOilPass.OilPassVehicleNo = OilPasss.OilPassVehicleNo;
                            objOilPass.DepartmentId = OilPasss.DepartmentId;
                            objOilPass.MsebDetailId = OilPasss.MsebDetailId;
                            break;
                        case BaseState.Added:

                            OilPasss.OilPassDetailId = GenrateID();
                            clientContext.OilPasss.Add(OilPasss);

                            break;
                    }

                    clientContext.SaveChanges();
                    OilPassDetID = OilPasss.OilPassDetailId;
                    OilDetail od = new OilDetail();
                    od.EntityState = BaseState.Added;
                    od.OilPassDetailId = OilPassDetID;
                    od.TotalOilRecieved = TotalOilRecieved;
                    od.TotalOilRequird = 0;
                    od.TotalOil = TotalOilRecieved;

                    SaveOilDetails(od);



                    return true;

                }



                return true;
            }
            catch (Exception ex)
            {

                LogHelper.LogHelper.ExceptionError(ex);

            }
            return false;
        }


        #endregion

        #region Print
        public string PrintGetPass(int GatePassDetailID)
        {
            StringBuilder data = new StringBuilder();


            var dgtc = from gtc in clientContext.GatePassTranfomerConfigurations.Where(x => x.GatePassDetailId == GatePassDetailID).OrderBy(x => x.TrasfomerJob_no)
                       select new
                       {
                           GatePassTranfomerConfigurationId = gtc.GatePassTranfomerConfigurationId,
                           OilSupplied = gtc.OilSupplied,
                           IsTrasfomerWithoutOil = gtc.IsTrasfomerWithoutOil,
                           TranfomserStatus = gtc.TranfomserStatus,
                           TrasfomerJob_no = gtc.TrasfomerJob_no,
                           TranformerDetail = clientContext.TranformerDetails.FirstOrDefault(x => x.TranformerDetailsId == gtc.TranformerDetailId),
                           GatePassDetail = clientContext.GatePassDetails.FirstOrDefault(x => x.GatePassDetailId == gtc.GatePassDetailId),
                           Department = clientContext.Departments.FirstOrDefault(x => x.DepartmentId == gtc.LastDepartmentId)
                       };
            string path = @"D:\TestProjects\TateElectricals\TateElectricals\TateElectricals\PrintReceipt.html";

            // This text is added only once to the file.
            if (File.Exists(path))
            {
                data.Append(File.ReadAllText(path));
            }
            string rw = "";
            foreach (var item in dgtc)
            {
                string rw1 = "<tr><td style = 'text-align:center;width:100px;'> " + item.TrasfomerJob_no + " </td><td style = 'text-align: center; '> " + item.TranformerDetail.TranfomerMake + " </td><td style = 'text-align: center;'> " + item.TranformerDetail.TransformerType + " </td><td style = 'text-align: center; '> " + item.TranformerDetail.TransfomerSr_no + " </td><td style = 'text-align: center; '> " + item.TranformerDetail.TransformerOilCapacity + " </td><td style = 'text-align: center; '> " + item.OilSupplied + " </td><tr>";
                rw = rw += rw1;
            }
            data = data.Replace("{VehicalNo}", dgtc.FirstOrDefault().GatePassDetail.GatePassVehicleNo);
            data = data.Replace("{DriverName}", dgtc.FirstOrDefault().GatePassDetail.GatePassSendersName);
            data = data.Replace("{GatePassDate}", dgtc.FirstOrDefault().GatePassDetail.GatePassDate.ToShortDateString());
            data = data.Replace("{GatePassTokenNo}", dgtc.FirstOrDefault().GatePassDetail.GatePassSr_no.ToString());
            data = data.Replace("TransformerRows", rw);
            return data.ToString();

        }
        #endregion
    }
}
