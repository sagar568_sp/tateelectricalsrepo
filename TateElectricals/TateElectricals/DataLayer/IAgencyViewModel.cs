﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TateElectricals.DataModels;

namespace TateElectricals.DataLayer
{
   public interface IAgencyViewModel
    {
        SystemUser SelectedUser { get; set; }
    }
}
