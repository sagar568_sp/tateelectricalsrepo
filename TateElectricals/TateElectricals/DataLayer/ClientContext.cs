﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TateElectricals.DataModels;

namespace TateElectricals.DataLayer
{
    public class ClientContext : DbContext
    {
        public ClientContext(DbContextOptions<ClientContext> options) : base(options)
        {
            
        }
        public DbSet<SystemUser> SystemUsers { get; set; }
        public DbSet<Department> Departments { get; set; }
        public DbSet<ClientInfo> ClientInfos { get; set; }

        public DbSet<GatePassDetail> GatePassDetails { get; set; }

        public DbSet<GatePassTranfomerConfiguration> GatePassTranfomerConfigurations { get; set; }
        public DbSet<MsebDetail> MsebDetails { get; set; }

        public DbSet<TranformerDetail> TranformerDetails { get; set; }

        public DbSet<OilPassDetail> OilPasss { get; set; }

        public DbSet<OilDetail> OilDetails { get; set; }

        public DbSet<TranformerWarratyDetail> TranformerWarratyDetails { get; set; }
    }


}
