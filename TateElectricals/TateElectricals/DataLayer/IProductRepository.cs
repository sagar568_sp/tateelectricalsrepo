﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TateElectricals.DataModels;

namespace TateElectricals.DataLayer
{
    public interface IProductRepository
    {
        #region Getmethods
        SystemUser ValidateSystemUser(string UserName, string Password);
        ClientInfo GetClientInfo(int ClientInfoId);

       // object GetDepartments();
         object GetSystemUser(int id);
        List<MsebDetail> GetMSED();
        object GetTransformr(string Date = "", int JobNo = 0, int TokenNo = 0, int currentPage = 1, int PageSize = 0);
        object GetOilDetails(int currentPage = 1, int PageSize = 0);
        string PrintGetPass(int GatePassDetailID);
        #endregion

        #region Postmethods
        bool SaveSystemUser(SystemUser systemUser);
      //  bool SaveDepartments(Department department,SystemUser systemUser);
        bool SaveMsebDetail(MsebDetail MsebDetail);
        GatePassTranfomerConfiguration SaveGatePass(GatePassDetail GPDetail, TranformerDetail TranDetail, ref GatePassTranfomerConfiguration gtc);

        bool SaveOilPass(OilPassDetail OilPasss, int TotalOilRecieved);
        #endregion
    }
}
