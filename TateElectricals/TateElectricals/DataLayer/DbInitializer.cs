﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TateElectricals.DataLayer
{
    public class DbInitializer
    {
        public static void Initialize(ClientContext context)
        {

            //context.Database.Migrate();
            context.Database.EnsureCreated();   //created database  
            if (!context.ClientInfos.Any())
            {
                context.ClientInfos.Add(new DataModels.ClientInfo { ClientInfoId = 12, ClientName = "Tate Electricals", ClientLogo = null, ClientAddress = "A/p bmt", ClientCity = "Baramati", ClientContact = 1446146, ClientEmail = "gggh@gmail.com", ClientLicense = 15 });
            }
            if (!context.SystemUsers.Any())
            {
                var user = new DataModels.SystemUser()
                {
                    SystemUserId = 1,
                    UserName = "admin",
                    Password = "1234",

                    ClientInfoId = 12,
                    DepartmentId = null,

                };
                user.SetPassword(user.Password);
                user.department = new DataModels.Department()
                {
                    DepartmentId=121,
                    DepartmentType = DataModels.DepartmentType.Agency,
                    DeptAddress="sad",
                    DeptCity ="bmt",
                    DeptCode="001",
                    DeptContact="1245",
                    DeptEmail="addf@gmail.com",
                    DeptName = "tetet"
                };
                context.SystemUsers.Add(user);
            }


            context.SaveChanges();
        }


    }
}
