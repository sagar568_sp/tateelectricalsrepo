﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace TateElectricals.LogHelper
{
    public class LogHelper
    {
        #region Field and Constant Member

        static internal string ApplicationPath = string.Empty;
        private static string _strLogFilePath = string.Empty;
        private static string _strLogFileName = string.Empty;
        private static string _filename = string.Empty;
        private static XmlDocument _xDoc;

        #endregion Field and Constant Member

        #region Member Functions

        /// <summary>
        /// Init the log file.
        /// </summary>
        /// <param name="applicationError">if set to <c>true</c> [application error].</param>
        private static void InitLogFile(bool applicationError)
        {
            //var oDate = DateTime.Now;
            //var intStart = (oDate.DayOfWeek - 1);
            //var intEnd = (7 - Convert.ToInt32(oDate.DayOfWeek));
            //if (oDate.DayOfWeek == 0)
            //{
            //    intStart = (oDate.DayOfWeek - 6);
            //    intEnd = Convert.ToInt32(oDate.DayOfWeek);
            //}

            var oDate = DateTime.Now;  //Mayur 04-02-2019
            var intStart = (DayOfWeek.Monday - oDate.DayOfWeek);
            var intEnd = (7 - Convert.ToInt32(oDate.DayOfWeek));
            if (oDate.DayOfWeek == 0)
            {
                intStart = (Convert.ToInt32(oDate.DayOfWeek) - 6);
                intEnd = Convert.ToInt32(oDate.DayOfWeek);
            }

            _strLogFilePath = Path.Combine("C:\\Logs\\", "logreport\\");
            //_strLogFilePath = Convert.ToString(ConfigurationManager.AppSettings["logreport"]);
            if (!Directory.Exists(_strLogFilePath))
                Directory.CreateDirectory(_strLogFilePath);

            var strStartDate = oDate.AddDays(Convert.ToDouble(intStart)).ToString("dd-MM-yyyy");
            var strEndDate = oDate.AddDays(intEnd).ToString("dd-MM-yyyy");

            var strInitialName = (applicationError ? "app_" : "log_");

            _filename = strInitialName + strStartDate + "_" + strEndDate;
            _strLogFileName = string.Format(_strLogFilePath + "{0}" + strStartDate + "_" + strEndDate + ".xml", strInitialName);

            LoadLogFile();
        }

        /// <summary>
        /// Check log File Size change by Mayur 22/11/2018
        /// </summary>
        /// <param name="_strLogFilePath"></param>
        public static void checkfilesize(string _strLogFilePath)
        {
            string filename;
            var files = Directory.GetFiles(_strLogFilePath, _filename + "*", SearchOption.AllDirectories);
            if (files.Length == 1)
            {
                filename = files[0];
                var filebyte = File.ReadAllBytes(filename);
                if (filebyte.Length >= 1074893)
                {
                    string file = _filename + " - " + files.Length + ".xml";
                    _strLogFileName = _strLogFilePath + file;
                }
                else
                    _strLogFileName = filename;
            }
            else
            {
                filename = files[files.Length - 2];
                var filebyte = File.ReadAllBytes(filename);
                if (filebyte.Length >= 1074893)
                {
                    //string file = _filename + " - " + files.Length + ".xml";
                    _strLogFileName = filename;//_strLogFilePath + file;
                    if (File.Exists(filename))
                        File.Delete(filename);
                }
                else
                    _strLogFileName = filename;
            }

            return;

        }
        /// <summary>
        /// Loads the log file.
        /// </summary>
        private static void LoadLogFile()
        {
            if (File.Exists(_strLogFileName))
                checkfilesize(_strLogFilePath);

            if (!File.Exists(_strLogFileName))
            {
                var xWriter = new XmlTextWriter(_strLogFileName, Encoding.UTF8) { Indentation = 4, IndentChar = ' ' };

                xWriter.Formatting = (Formatting)xWriter.Indentation;
                xWriter.WriteStartDocument();

                xWriter.WriteComment("Error log of web application.");
                xWriter.WriteStartElement("log");
                xWriter.WriteStartElement("errors");
                xWriter.WriteEndElement();
                //errors
                xWriter.WriteStartElement("entries");
                xWriter.WriteEndElement();
                //entries
                xWriter.WriteEndElement();
                //log

                xWriter.WriteEndDocument();

                xWriter.Flush();
                xWriter.Close();
            }

            if (!File.Exists(_strLogFileName) || _xDoc != null) return;
            _xDoc = new XmlDocument();
            _xDoc.Load(_strLogFileName);
        }

        /// <summary>
        /// Exceptions the error.
        /// </summary>
        /// <param name="exc">The exc.</param>
        public static void ExceptionError(Exception exc)
        {

            var ex = new Exception("no error");
            XmlElement xDateNode = null;
            var strDate = DateTime.Now.ToString("dd-MM-yyyy dddd");

            var strBuilder = new StringBuilder();

            try
            {
                if (exc != null)
                    ex = exc;

                InitLogFile(true);

                if (!File.Exists(_strLogFileName)) return;

                strBuilder.Append("Message: " + ex.Message + Environment.NewLine);
                strBuilder.Append("Target: ");
                strBuilder.Append(ex.TargetSite);
                strBuilder.Append(Environment.NewLine);
                strBuilder.Append("Stack: " + Environment.NewLine + ex.StackTrace + Environment.NewLine + Environment.NewLine);

                var strDetail = ex.ToString();
                var intSubIndex = strDetail.IndexOf("--- End of inner exception stack trace ---");
                if (intSubIndex > 0)
                    strBuilder.Append("Detail: " + Environment.NewLine + strDetail.Substring(0, intSubIndex).Trim());

                if (ex.InnerException != null)
                {
                    strBuilder.Append("Inner Message: " + ex.InnerException.Message + Environment.NewLine);
                    if (ex.InnerException.InnerException != null)
                        strBuilder.Append("Inner Message: " + ex.InnerException.InnerException.Message +
                                          Environment.NewLine);
                }

                var xNode = _xDoc.SelectSingleNode("descendant::errors");
                if (xNode != null)
                {
                    if (xNode.HasChildNodes)
                        xDateNode = (XmlElement)xNode.SelectSingleNode("descendant::error[@date='" + strDate + "']");

                    if (xDateNode == null)
                    {
                        xDateNode = _xDoc.CreateElement("error");
                        xDateNode.SetAttribute("date", strDate);
                        xNode.AppendChild(xDateNode);
                    }
                }

                if ((xDateNode != null))
                {
                    var xErrorNode = _xDoc.CreateElement("detail");
                    xErrorNode.SetAttribute("time", DateTime.Now.ToString("hh:mm:ss tt"));
                    xErrorNode.AppendChild(_xDoc.CreateCDataSection(strBuilder.ToString()));
                    xDateNode.AppendChild(xErrorNode);
                }

                _xDoc.Save(_strLogFileName);
            }
            catch (Exception)
            {
            }
            finally
            {
                _xDoc = null;
            }
        }

        /// <summary>
        /// Logs the error.
        /// </summary>
        /// <param name="strErrorMessage">The STR error message.</param>
        /// <param name="applicationError">if set to <c>true</c> [application error].</param>
        public static void LogError(string strErrorMessage, bool applicationError)
        {
            XmlElement xDateNode = null;
            var strDate = DateTime.Now.ToString("dd-MM-yyyy dddd");

            try
            {
                InitLogFile(applicationError);

                if (!File.Exists(_strLogFileName)) return;

                var xNode = _xDoc.SelectSingleNode("descendant::errors");
                if (xNode != null && xNode.HasChildNodes)
                    xDateNode = (XmlElement)xNode.SelectSingleNode("descendant::error[@date='" + strDate + "']");

                if (xDateNode == null)
                {
                    xDateNode = _xDoc.CreateElement("error");
                    xDateNode.SetAttribute("date", strDate);
                    if (xNode != null) xNode.AppendChild(xDateNode);
                }

                var xErrorNode = _xDoc.CreateElement("detail");
                xErrorNode.SetAttribute("time", DateTime.Now.ToString("hh:mm:ss tt"));
                xErrorNode.AppendChild(_xDoc.CreateCDataSection(strErrorMessage));
                xDateNode.AppendChild(xErrorNode);

                _xDoc.Save(_strLogFileName);
            }
            catch (Exception)
            {
            }
            finally
            {
                // Release objects.
                _xDoc = null;
            }
        }

        /// <summary>
        /// Logs the message.
        /// </summary>
        /// <param name="strMessage">The STR message.</param>
        public static void LogMessage(string strMessage)
        {
            XmlElement xDateNode = null;
            var strDate = DateTime.Now.ToString("dd-MM-yyyy dddd");

            try
            {
                InitLogFile(true);

                if (!File.Exists(_strLogFileName)) return;

                var xNode = _xDoc.SelectSingleNode("descendant::entries");
                if (xNode != null)
                {
                    if (xNode.HasChildNodes)
                        xDateNode = (XmlElement)xNode.SelectSingleNode("descendant::entry[@date='" + strDate + "']");

                    if (xDateNode == null)
                    {
                        xDateNode = _xDoc.CreateElement("entry");
                        xDateNode.SetAttribute("date", strDate);
                        xNode.AppendChild(xDateNode);
                    }
                }

                var xErrorNode = _xDoc.CreateElement("detail");
                xErrorNode.SetAttribute("time", DateTime.Now.ToString("hh:mm:ss tt"));
                xErrorNode.AppendChild(_xDoc.CreateTextNode(strMessage));
                if (xDateNode != null) xDateNode.AppendChild(xErrorNode);
                long len = new FileInfo(_strLogFileName).Length;
                string filesize = GetBytesReadable(len);
                //if (filesize == "GB" || filesize == "TB")
                //    SendMail(_strLogFileName);
                _xDoc.Save(_strLogFileName);

            }
            catch (Exception)
            {
            }
            finally
            {
                // Release objects.
                _xDoc = null;
            }
        }

        public static string GetBytesReadable(long i)
        {
            long absolute_i = (i < 0 ? -i : i);
            // Determine the suffix and readable value
            string suffix = string.Empty;
            double readable;
            if (absolute_i >= 0x40000000) // Gigabyte
            {
                suffix = "GB";
                readable = (i >> 20);
            }
            else if (absolute_i >= 0x10000000000) // Terabyte
            {
                suffix = "TB";
                readable = (i >> 30);
            }
            return suffix;
        }

        

        public static bool checknetconnection()
        {
            try
            {
                using (var client = new WebClient())
                using (var stream = client.OpenRead("http://www.google.com"))
                {
                    return true;
                }
            }
            catch
            {
                return false;
            }
        }


        #endregion Member Functions
    }

}
