﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using TateElectricals.Models;
using TateElectricals.DataModels;
using TateElectricals.DataLayer;
namespace TateElectricals.Controllers
{
    public class BasicOperationsController : Controller
    {
        private readonly ILogger<BasicOperationsController> _logger;
        private IProductRepository IProductRepository
        { get; set; }
        public BasicOperationsController(ILogger<BasicOperationsController> logger, IProductRepository productRepository)
        {
           
            IProductRepository = productRepository;
            _logger = logger;
            if (SessionManager.SessionManager.Current.AgencyViewModel == null)
            {
                SessionManager.SessionManager.Current = new SessionManager.SessionManager();
                AgencyViewModel = new AgencyViewModel();
                SessionManager.SessionManager.Current.AgencyViewModel = AgencyViewModel;
            }
            else
            {
                AgencyViewModel = SessionManager.SessionManager.Current.AgencyViewModel;
            }
        }
        public IAgencyViewModel AgencyViewModel { get; set; }

        [Route("MyHome")]
        public IActionResult HomePage()
        {
            try
            {
                return View();
            }
            catch (Exception ex)
            {
                return View();
            }

        }

        [Route("AddGetPass")]
        public IActionResult AddGetPass()
        {
            try
            {
                ViewBag.MSED= IProductRepository.GetMSED();
                ViewBag.deptinfo = AgencyViewModel.SelectedUser.department;
                return PartialView("GetPass");
            }
            catch (Exception ex)
            {
                return View();
            }

        }
        [Route("SaveGatePass")]
        public IActionResult SaveGatePass(GatePassDetail GPDetail, TranformerDetail TranDetail, GatePassTranfomerConfiguration gtc)
        {
            GatePassTranfomerConfiguration td = IProductRepository.SaveGatePass(GPDetail, TranDetail, ref gtc);
            return Json(new { td });
        }

        [Route("AddGetPassOil")]
        public IActionResult GetPassOil()
        {
            try
            {
                ViewBag.MSED = IProductRepository.GetMSED();
               // ViewBag.deptinfo = HomeController.dept;
                
                //return  View();
                return PartialView("GetPassOil");
            }
            catch (Exception ex)
            {
                return View();
            }

        }
        [Route("SaveGetPassOil")]
        public IActionResult SaveGetPassOil(OilPassDetail OilPasss, int TotalOilRecieved)
        {
            OilPasss.DepartmentId = 6271539;//take from login user
            bool isSaved = IProductRepository.SaveOilPass( OilPasss, TotalOilRecieved);
            return Json(new { isSaved });

        }
        [Route("GetOilDetails")]
        public IActionResult GetOilDetails(int currentPage = 1, int PageSize = 2)
        {
            int id = 0;// take i from session 
            object Dlist = IProductRepository.GetOilDetails(currentPage, PageSize);
            return Json(new { Dlist });
        }

        [Route("Home")]
        public IActionResult Home()
        {
            try
            {
                return View();
            }
            catch (Exception ex)
            {
                return View();
            }

        }

        [Route("GetTrans")]
        public IActionResult GetTrans(string Date = "", int JobNo = 0, int TokenNo = 0, int currentPage = 1, int PageSize = 2)
        {
            try
            {
                var Dlist = IProductRepository.GetTransformr(Date, JobNo, TokenNo, currentPage, PageSize);
                return Json(new { Dlist });
            }
            catch (Exception ex)
            {
                return View();
            }

        }
        [Route("ViewITR")]
        public IActionResult TRInfo()
        {
            try
            {
                
                return View();
            }
            catch (Exception ex)
            {
                return View();
            }

        }

        [Route("AdminView")]
        public IActionResult AdminHome()
        {
            try
            {
                return View();
            }
            catch (Exception ex)
            {
                return View();
            }

        }

        [Route("Users")]
        public IActionResult vwUser()
        {
            try
            {
                return View();
            }
            catch (Exception ex)
            {
                return View();
            }

        }

        [Route("Departments")]
        public IActionResult vwDepartment()
        {
            return View();
        }
        [Route("SaveDepartments")]
        public IActionResult SaveDepartments( SystemUser systemuser)
        {
            bool isSaved = IProductRepository.SaveSystemUser( systemuser);
            return Json(new { isSaved });
        }
        [Route("GetDepartments")]
        public IActionResult GetDepartments()
        {
            int id = 0;// take i from session 
             object Dlist = IProductRepository.GetSystemUser(id);
            return Json(new { Dlist });
        }

        #region MSED

        [Route("MSED")]
        public IActionResult vwmsed()
        {
            return View();

        }

        [Route("SaveMSED")]
        public IActionResult SaveMSED(MsebDetail MSED)
         {
            bool isSaved = IProductRepository.SaveMsebDetail(MSED);
            return Json(new { isSaved });
        }
        [Route("GetMSED")]
        public IActionResult GetMSED()
        {
            List<MsebDetail> Dlist = IProductRepository.GetMSED();
            return Json(new { Dlist });
        }
        //[Route("DeleteMSED")]
        //public IActionResult DeleteMSED(int MSEDID)
        //{
        //    MsebDetail obj = new MsebDetail() { MsebDetailID = MSEDID ,EntityState=EntityState.Deleted};
        //     bool isSaved = IProductRepository.SaveMsebDetail(obj);
        //    return Json(new { isSaved}) ;
        //}
        #endregion


        #region Print
        [Route("PrintGetPass")]
        public IActionResult PrintGetPass(int GetpassdetailID)
        {
            
            string Dlist = IProductRepository.PrintGetPass(GetpassdetailID);
            return Json(new { Dlist });
        }

        #endregion

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }



        








    }
}
