﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using TateElectricals.Models;
using System.Web;
using TateElectricals.DataLayer;
using TateElectricals.DataModels;

namespace TateElectricals.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

      // public static List<Department> dept = new List<Department>();
        private IProductRepository IProductRepository
        { get; set; }
        public HomeController(ILogger<HomeController> logger, IProductRepository productRepository)
        {
            IProductRepository = productRepository;
            _logger = logger;
            if (SessionManager.SessionManager.Current == null)
            {
                SessionManager.SessionManager.Current = new SessionManager.SessionManager();
                AgencyViewModel = new AgencyViewModel();
                SessionManager.SessionManager.Current.AgencyViewModel = AgencyViewModel;
            }
            else
            {
                AgencyViewModel = SessionManager.SessionManager.Current.AgencyViewModel;
            }

            if (AgencyViewModel == null)
            {
                RedirectToPagePermanent("~/Home/LoginPage");
            }
        }
        public IAgencyViewModel AgencyViewModel { get; set; }
        public IActionResult LoginPage()
        {   
            return View();
        }

        [Route("Logout")]
        public IActionResult LogOut()
        {
            // dept = new List<Department>();
            AgencyViewModel.SelectedUser = null;
            return Json(new { IsLoggedIn = true });
        }

        public IActionResult LoginModel(string uName,string uPass)
        {
            //string uPass = "1234";
            var d = IProductRepository.ValidateSystemUser(uName, uPass);

            if (d != null) { 
               // dept.Add(d.department);
                AgencyViewModel.SelectedUser = d;
                
            return Json(new { IsLoggedIn = AgencyViewModel.SelectedUser.department.DepartmentType.ToString() });
            }
            else return Json(new { IsLoggedIn = "No Valid" });
           // if (uName== "admin")
           // return Json(new { IsLoggedIn = "admin" });
           //else if(uName=="pcontrol")
           //     return Json(new { IsLoggedIn = "Agency" });
          // else return Json(new { IsLoggedIn = "False" });

        }
        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
