﻿
var ActionUrl;
var ModifyDeptID = null;
var ModifyDeptUserId = null;
var ModifyMSEDid = null;
var IsValidForm = true;
$(document).ready(function () {
    $('#topMain ul>li').click(function (e) {
        NavMenu(this.id);

       
    });

    //admin script
    $(document).on('click', '#SaveDpt', function (e) {
        ValidateForm('DeptForm');
        if (IsValidForm) {
            let Deptcode = document.getElementById('Deptcode').value;
            let DeptName = document.getElementById('DeptName').value;
            let DeptType = document.getElementById('DeptType').value;
            let DeptCity = document.getElementById('DeptCity').value;
            let DeptEmail = document.getElementById('DeptEmail').value;
            let DeptContact = document.getElementById('DeptContact').value;
            let DeptAddress = document.getElementById('DeptAddress').value;
            let DeptUser = document.getElementById('DeptUser').value;
            let DeptPass = document.getElementById('DeptPass').value;
            debugger;
            var deptDetails = new Object();
            // deptDetails.DepartmentId = 1;
            deptDetails.EntityState = "Added";
            deptDetails.DeptCode = Deptcode;
            deptDetails.DeptName = DeptName;
            //deptDetails.DeptLogo = "";
            deptDetails.DeptAddress = DeptAddress;
            deptDetails.DeptCity = DeptCity;
            deptDetails.DeptEmail = DeptEmail;
            deptDetails.DeptContact = DeptContact;
            deptDetails.DepartmentType = DeptType;
            //deptDetails.SystemUserID = "test";
            var systemuser = new Object();
            systemuser.UserName = DeptUser;
            systemuser.Password = DeptPass;
            systemuser.EntityState = "Added";
            if (document.getElementById('SaveDpt').value == 'Update' && ModifyDeptID != null && ModifyDeptUserId != null) {
                deptDetails.EntityState = "Modified";
                systemuser.EntityState = "Modified";
                deptDetails.DepartmentId = ModifyDeptID;
                systemuser.SystemUserId = ModifyDeptUserId;
                systemuser.DepartmentId = ModifyDeptID;
            }
            systemuser.department = deptDetails;
            $.ajax({
                data: { DeptDetails: deptDetails, systemuser: systemuser },
                type: "POST",
                dataType: "json",
                // contentType: "application/json; charset=utf-8",
                url: window.location.origin + "/SaveDepartments",
            }).done(function (result) {
                if (ModifyDeptID != null) alert('Agency Modifed Successfully');
                else alert('Agency Saved Successfully');
                GetDepartments();
                ModifyDeptID = null;
                ModifyDeptUserId = null;
                document.getElementById('DeptForm').reset();
                $('#DeptForm').find('input,textarea').css('border', '');
                $('#DeptForm').find('span').remove();
                document.getElementById('SaveDpt').value = 'Save';
                $('#additem').hide();
                $('#listtbl').show();
                $('#btnAdd').show();
            }).fail(function (textStatus, errorThrown) {
                alert(textStatus);
            });
        }

    });
    $(document).on('click', '#SaveMsedDiv', function (e) {
        debugger;
        ValidateForm('MsedDiv');
        if (IsValidForm) {
            let Msedcode = document.getElementById('Msedcode').value;
            let MsedName = document.getElementById('MsedName').value;
            let MsedLoc = document.getElementById('MsedLoc').value;
            let MsedCity = document.getElementById('MsedCity').value;
            let MsedEmail = document.getElementById('MsedEmail').value;
            let MsedContact = document.getElementById('MsedContact').value;
            let MsedAddress = document.getElementById('MsedAddress').value;

            var objMsebDetail = new Object();
            objMsebDetail.EntityState = "Added";
            objMsebDetail.MsebName = MsedName;
            objMsebDetail.MsebCode = Msedcode;
            objMsebDetail.MsebAddress = MsedAddress;
            objMsebDetail.MsebCity = MsedCity;
            objMsebDetail.MsebEmail = MsedEmail;
            objMsebDetail.MsebContact = MsedContact;
            objMsebDetail.MsebLocation = MsedLoc;


            if (document.getElementById('SaveMsedDiv').value == 'Update' && ModifyMSEDid != null) {
                objMsebDetail.EntityState = "Modified";
                objMsebDetail.MsebDetailID = ModifyMSEDid;
            }

            debugger;
            $.ajax({
                data: { MSED: objMsebDetail },
                type: "POST",
                //dataType: "json",
                //contentType: "application/json; charset=utf-8",
                url: window.location.origin + "/SaveMSED"
            }).done(function (result) {
                if (ModifyMSEDid != null) alert('MSED Modifed Successfully');
                else alert('MSED Saved Successfully');
                GetMSED();
                document.getElementById('MsedDiv').reset();
                $('#MsedDiv').find('input,textarea').css('border', '');
                $('#MsedDiv').find('span').remove();
                ModifyMSEDid = null;
                document.getElementById('SaveMsedDiv').value == 'Save';
                $('#additem').hide();
                $('#listtbl').show();
                $('#btnAdd').show();
            }).fail(function (textStatus, errorThrown) {
                alert(textStatus);
            });
        }

    });
    //admin script
    //agency script
    $(document).on('click', '#SaveTPass', function (e) {
        ValidateForm('TGatePass');
        if (IsValidForm) {
            let TMsed = document.getElementById('TMsed').value;
            let TgPassNo = document.getElementById('TgPassNo').value;
            let TDate = document.getElementById('TDate').value;
            let TvNo = document.getElementById('TvNo').value;
            let TDName = document.getElementById('TDName').value;
            let ATunit = document.getElementById('ATunit').value;
            //let TjobNo = document.getElementById('TjobNo').value;
            let TMake = document.getElementById('TMake').value;
            let TCapacity = $('#TCapacity option:selected').html();
            let TSNo = document.getElementById('TSNo').value;
            let TROil = document.getElementById('TROil').value;
            let TRecOil = document.getElementById('TRecOil').value;
            let OmrName = document.getElementById('OmrName').value;
            let GPRemark = document.getElementById('GPRemark').value;
            
            var gtpID = $('#hdfGetPassID').val();

            var GPDetail = new Object();
            if (gtpID == 0 || gtpID == null || gtpID == undefined)
                GPDetail.EntityState = "Added";
            else {
                GPDetail.EntityState = "Modified";
                GPDetail.GatePassDetailId = gtpID;
            }
            GPDetail.GatePassSr_no = TgPassNo;
            GPDetail.GatePassDate = TDate;
            GPDetail.GatePassRemark = GPRemark;
            GPDetail.GatePassReciversName = OmrName;
            GPDetail.GatePassSendersName = TDName;
            GPDetail.GatePassVehicleNo = TvNo;
            GPDetail.DepartmentId = ATunit;
            GPDetail.MsebDetailId = TMsed;
            GPDetail.IsGatePassWithoutOil = "";


            var TranDetail = new Object();
            var trID = $('#hdfTranID').val();

            if (trID == 0 || trID == null || trID == undefined)
                TranDetail.EntityState = "Added";
            else {
                TranDetail.EntityState = "Modified";
                TranDetail.TranformerDetailsId = trID;
            }
            /// TranDetail.TransformerCapacity = TCapacity;
            TranDetail.TransformerType = TCapacity;
            TranDetail.TransformerOilCapacity = TROil;

            TranDetail.TranfomerMake = TMake;
            TranDetail.TransfomerSr_no = TSNo;
            TranDetail.TransfomerService_no = "";

            var gtc = new Object();
            gtc.EntityState = "Added";
            gtc.LastDepartmentId = ATunit;
            if (TRecOil != 0 && TRecOil != null) {
                gtc.IsTrasfomerWithoutOil = 0;
            }
            else {
                gtc.IsTrasfomerWithoutOil = 1;
            }
            gtc.OilSupplied = TRecOil;
            /* gtc.TrasfomerJob_no = TjobNo;*/

            debugger;
            $.ajax({
                data: { GPDetail: GPDetail, TranDetail: TranDetail, gtc: gtc },
                type: "POST",
                dataType: "json",
                // contentType: "application/json; charset=utf-8",
                url: window.location.origin + "/SaveGatePass",
            }).done(function (result) {
                //Add Div id to append result

                debugger;
                $('#hdfGetPassID').val(result.td.GatePassDetailId);

                $('#tbodyTRdet').append("<tr><td>" + result.td.TrasfomerJob_no + "</td><td>" + result.td.tranformerDetail.TranfomerMake + "</td><td>" + result.td.tranformerDetail.TransformerType + "</td><td>" + result.td.tranformerDetail.TransfomerSr_no + "</td><td>" + result.td.tranformerDetail.TransformerOilCapacity + "</td><td>" + result.td.OilSupplied + "</td><td><a><span class='fa fa-trash'></span></a></td></tr>");
                $('#tbodyTRdet').parent().show();
                $('#divPrint').show();
                // $('#Pview').empty();

            }).fail(function (textStatus, errorThrown) {
                debugger;
                alert(textStatus);
            });
        }

    });
    $(document).on('click', '#SaveOil', function (e) {
        
        ValidateForm('OilGatePass');
        if (IsValidForm) {
            let Omsed = document.getElementById('Omsed').value;
            let OGetPass = document.getElementById('OGetPass').value;
            let Odate = document.getElementById('Odate').value;
            let OVno = document.getElementById('OVno').value;
            let ODName = document.getElementById('ODName').value;
            let OQ = document.getElementById('OQ').value;
            let OmrName = document.getElementById('OmrName').value;
            let EntityState = "Added";
            
            var objOilPass = new Object;
            objOilPass.OilPassSr_no = OGetPass;
            objOilPass.OilPassDate = Odate;
            objOilPass.OilPassRemark = "NA";
            objOilPass.OilPassReciversName = OmrName;
            objOilPass.OilPassSendersName = ODName;
            objOilPass.OilPassVehicleNo = OVno;
            /*objOilPass.DepartmentId = DepartmentId;*/
            objOilPass.MsebDetailId = Omsed;
            objOilPass.EntityState = EntityState;
            debugger;
            $.ajax({

                data: { OilPasss: objOilPass, TotalOilRecieved: OQ },
                type: "POST",
                dataType: "json",
                // contentType: "application/json; charset=utf-8",
                url: window.location.origin + "/SaveGetPassOil",
            }).done(function (result) {
                debugger;
                //Add Div id to append result
                document.getElementById('OilGatePass').reset();
                alert("Data Saved");
               // $('#Pview').empty().append(result)
            }).fail(function (textStatus, errorThrown) {
                alert(textStatus);
            });
        }
        
    });
    //agency script
})
function NavMenu(id) {
    debugger;

    var loc = window.location;

    switch (id) {

        case "logoHome":
            ActionUrl = loc.origin + "/Home";
            break;
        case "Home":
            ActionUrl = loc.origin + "/Home";
            break;
        case "gPassT":
            ActionUrl = loc.origin + "/AddGetPass";
            //  ActionUrl = loc.origin +  "/BasicOperations/GetPass";
            break;
        case "gPassO":
            ActionUrl = loc.origin + "/AddGetPassOil";
            break;
        case "TracIT":
            ActionUrl = loc.origin + "/ViewITR";
            break;
        case "cDept":
            ActionUrl = loc.origin + "/Departments";
            break;
        case "cmsed":
            ActionUrl = loc.origin + "/MSED";
            break;
    }
    $.ajax({
        data: {},
        type: "GET",
        dataType: "html",
        // contentType: "application/json; charset=utf-8",
        url: ActionUrl
    }).done(function (result) {
        //Add Div id to append result
        $('#Pview').empty().append(result)
    }).fail(function (textStatus, errorThrown) {
        alert(textStatus);
    });

}

function ValidateForm(id) {
    //take id and regx from input field

    IsValidForm = true;
    $("#" + id).find('input,textarea,select').each(function (index, element) {

        var type = element.type;
        //var msg = element.msg;
        var msg = element.getAttribute('msg');
        var checkRegx = new RegExp(element.getAttribute('regx'));
        var checkType = ["text", "password", "textarea", "select-one", "date"];
        if (checkType.includes(type)) {
                        if (element.value == "") {
                //(!checkRegx.test(element.value))
                IsValidForm = false;
                element.style.border = "2px solid red";
                if (element.nextElementSibling == null) {
                    let ele = document.createElement('span');
                    ele.innerText = msg;
                    ele.style.color = "red";
                    element.after(ele);
                }
            }
            else {
                if (element.nextElementSibling != null)
                    element.nextSibling.remove();
                element.style.border = '';
            }
        }
        //switch (type) {
        //    case "text":
        //        if (element.value = "" || (!regx.test(iValue)))
        //        {
        //            element.style.border = "2px solid red";
        //            let ele = document.createElement('span').innerText = msg.style.color = "red";
        //            element.after(ele);
        //        }
        //        else
        //            element.nextSibling.remove();
        //        break;
        //    case "password":
        //        if (element.value = "" || (!regx.test(iValue))) {
        //            element.style.border = "2px solid red";
        //            let ele = document.createElement('span').innerText = msg.style.color = "red";
        //            element.after(ele);
        //        }
        //        else
        //            element.nextSibling.remove();
        //        break;
        //    case "textarea":
        //        if (element.value = "" || (!regx.test(iValue))) {
        //            element.style.border = "2px solid red";
        //            let ele = document.createElement('span').innerText = msg.style.color = "red";
        //            element.after(ele);
        //        }
        //        else
        //            element.nextSibling.remove();
        //        break;


        //}
    });
}
function GetDepartments() {
    $('#listtbl').find('tbody').empty();
    debugger;
    $.ajax({
        data: {},
        type: "GET",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        url: window.location.origin + "/GetDepartments",
    }).done(function (result) {
        var DeptData = result.Dlist;
        var row = "";
        for (var i = 0; i < DeptData.length; i++) {
            var Dtype = DeptData[i].DepartmentType == 0 ? 'Agency' : 'Service';
            row = "<tr><td>" + DeptData[i].DeptCode + "</td><td>" + DeptData[i].DeptName + "</td> <td>" + Dtype + "</td><td>" + DeptData[i].DeptCity + "</td><td>" + DeptData[i].DeptEmail + "</td><td>" + DeptData[i].DeptContact + "</td><td>" + DeptData[i].DeptAddress + "</td> <td><a><i class='fa fa-edit ml-10' onclick='ModifyDept (" + DeptData[i].DepartmentId + "," + DeptData[i].SystemUserID + ")'></i></a></td>";
            $('#listtbl').find('tbody').append(row);
        }

    }).fail(function (textStatus, errorThrown) {
        alert(textStatus);
    });
}

function GetOilDetails() {
    $('#listtbl').find('tbody').empty();
    var cp = $("#hdfcurrentPage").val();
    debugger;
    $.ajax({
        data: { currentPage: cp, PageSize: 2},
        type: "GET",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        url: window.location.origin + "/GetOilDetails",
    }).done(function (result) {
        var DeptData = result.Dlist.Item1;
        var row = "";
        debugger;
        for (var i = 0; i < DeptData.length; i++) {
            
            if (DeptData[i].GetPassDet != null) {
                row = "<tr><td>" + DeptData[i].GatePassMSED.MsebName + "</td><td>" + DeptData[i].GetPassDet.GatePassSr_no + "</td> <td>" + new Date(DeptData[i].GetPassDet.GatePassDate).toLocaleDateString('en-TT') + "</td><td>" + DeptData[i].GetPassDet.GatePassVehicleNo + "</td><td>" + DeptData[i].GetPassDet.GatePassSendersName + "</td><td>" + DeptData[i].TotalOilRecieved + "</td><td>" + DeptData[i].TotalOilRequird + "</td><td>With Transformer</td><td>" + DeptData[i].GetPassDet.GatePassReciversName + "</td>";
            }
            else {
                row = "<tr><td>" + DeptData[i].OilPassMSED.MsebName + "</td><td>" + DeptData[i].OilPassDet.OilPassSr_no + "</td> <td>" + new Date(DeptData[i].OilPassDet.OilPassDate).toLocaleDateString('en-TT') + "</td><td>" + DeptData[i].OilPassDet.OilPassVehicleNo + "</td><td>" + DeptData[i].OilPassDet.OilPassSendersName + "</td><td>" + DeptData[i].TotalOilRecieved + "</td><td>" + DeptData[i].TotalOilRequird + "</td><td>Without Transformer</td><td>" + DeptData[i].OilPassDet.OilPassReciversName + "</td> ";
            }
            $('#listtbl').find('tbody').append(row);

            var TotalCount = parseInt(result.Dlist.Item2);
            var TotalPages = parseInt(result.Dlist.Item3);
            var PageSize = parseInt(result.Dlist.Item4);

            $('#trnPagn').text('');


            var lp = TotalPages;
            if (cp > 1) {
                $('#trnPagn').append('<li class="page-item" id="pageindexPre" onclick="Paging(this.id,0);"><a class="page-link" tabindex="-1">Previous</a></li> ');

            }
            else if (cp == 1) {
                $('#trnPagn').append('<li class="page-item disabled" id="pageindexPre" onclick="Paging(this.id,0);"><a class="page-link" tabindex="-1">Previous</a></li> ');

            }
            debugger;
            for (var j = 1; j <= TotalPages; j++) {
                var list = "";
                if (j == cp) {

                    list = "<li class='page-item active' id='pageindex" + j + "'  onclick='Paging(this.id, " + j + ");'><a class='page-link'>" + j + "</a></li>";
                }
                else {
                    list = "<li class='page-item' id='pageindex" + j + "'  onclick='Paging(this.id, " + j + ");'><a class='page-link'>" + j + "</a></li>";
                }
                $('#trnPagn').append(list);

            }

            if (TotalPages == cp) {
                $('#trnPagn').append('<li class="page-item disabled" id="pageindexNxt" onclick="Paging(this.id,4);" ><a class="page-link">Next</a></li>');

            }
            else if (TotalPages > cp) {
                $('#trnPagn').append('<li class="page-item" id="pageindexNxt" onclick="Paging(this.id,4);" ><a class="page-link">Next</a></li>');

            }

        }

    }).fail(function (textStatus, errorThrown) {
        alert(textStatus);
    });
}

function ModifyDept(Did, DuserID) {
    debugger;
    $('#additem').show();
    $('#btnAdd').hide();
    $('#listtbl').hide();
    document.getElementById('SaveDpt').value = 'Update'
    ModifyDeptID = Did;
    ModifyDeptUserId = DuserID;
    var tRow = event.currentTarget.parentNode.parentNode.parentNode.children;
    document.getElementById('Deptcode').value = tRow[0].innerText;
    document.getElementById('DeptName').value = tRow[1].innerText;
    document.getElementById('DeptCity').value = tRow[3].innerText;
    document.getElementById('DeptEmail').value = tRow[4].innerText;
    document.getElementById('DeptContact').value = tRow[5].innerText;
    document.getElementById('DeptAddress').value = tRow[6].innerText;
}

function GetMSED() {
    debugger;

    $('#listtbl').find('tbody').empty();
    $.ajax({
        data: {},
        type: "GET",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        url: window.location.origin + "/GetMSED",
    }).done(function (result) {
        var DeptData = result.Dlist;
        var row = "";
        for (var i = 0; i < DeptData.length; i++) {
            row = "<tr><td>" + DeptData[i].MsebCode + "</td><td>" + DeptData[i].MsebName + "</td> <td>" + DeptData[i].MsebLocation + "</td><td>" + DeptData[i].MsebCity + "</td><td>" + DeptData[i].MsebEmail + "</td><td>" + DeptData[i].MsebContact + "</td><td>" + DeptData[i].MsebAddress + "</td> <td><a><i class='fa fa-edit ml-10' onclick='ModifyMSED (" + DeptData[i].MsebDetailID + ")'></i></a></td>";
            $('#listtbl').find('tbody').append(row);
        }

    }).fail(function (textStatus, errorThrown) {
        alert(textStatus);
    });
}
function ModifyMSED(id) {
    $('#additem').show();
    $('#btnAdd').hide();
    $('#listtbl').hide();
    document.getElementById('SaveMsedDiv').value = 'Update'
    ModifyMSEDid = id;
    var tRow = event.currentTarget.parentNode.parentNode.parentNode.children;
    document.getElementById('Msedcode').value = tRow[0].innerText;
    document.getElementById('MsedName').value = tRow[1].innerText;
    document.getElementById('MsedLoc').value = tRow[2].innerText;
    document.getElementById('MsedCity').value = tRow[3].innerText;
    document.getElementById('MsedEmail').value = tRow[4].innerText;
    document.getElementById('MsedContact').value = tRow[5].innerText;
    document.getElementById('MsedAddress').value = tRow[6].innerText;
}
function DeleteMSED(id) {
    //var objMsebDetail = new Object();
    //objMsebDetail.EntityState = "Deleted";
    //objMsebDetail.MsebDetailID = id;
    $.ajax({
        data: { MSEDID: id },
        type: "POST",
        dataType: "json",
        //contentType: "application/json; charset=utf-8",
        url: window.location.origin + "/DeleteMSED"
    }).done(function (result) {
        if (result.isSaved) {
            alert('MSED Deleted Successfully');
            GetMSED();
        } else alert('MSED Not Deleted Successfully');
    }).fail(function (textStatus, errorThrown) {
        alert(textStatus);
    });
}

function getCurrentDate() {
    debugger;
    var now = new Date();
    
    var day = ("0" + now.getDate()).slice(-2);
    var month = ("0" + (now.getMonth() + 1)).slice(-2);

    var today = now.getFullYear() + "-" + month + "-" + day;

    return today;
}

